<?php

include(dirname(__FILE__).'/../../bootstrap/functional.php');

$browser = new JobeetTestFunctional(new sfBrowser());
$browser->loadData();

$browser->info('1 - The homepage')->
    get('/')->
    with('request')->begin()->
    isParameter('module', 'job')->
    isParameter('action', 'index')->
    end()->
    with('response')->begin()->
    info('  1.1 - Expired jobs are not listed')->
    checkElement('.jobs td.position:contains("expired")', false)->
    end()
;

$browser->info('3 - Post a Job page')->
    info('  3.1 - Submit a Job')->

    get('/job/new')->
    with('request')->begin()->
    isParameter('module', 'job')->
    isParameter('action', 'new')->
    end()->

    click('Preview your job', array('job' => array(
        'company'      => 'Sensio Labs',
        'url'          => 'http://www.sensio.com/',
        'logo'         => sfConfig::get('sf_upload_dir').'/jobs/sensio-labs.gif',
        'position'     => 'Developer',
        'location'     => 'Atlanta, USA',
        'description'  => 'You will work with symfony to develop websites for our customers.',
        'how_to_apply' => 'Send me an email',
        'email'        => 'for.a.job@example.com',
        'is_public'    => false,
    )))->

    with('request')->begin()->
    isParameter('module', 'job')->
    isParameter('action', 'create')->
    end()
;