<?php
// auto-generated by sfAutoloadConfigHandler
// date: 2014/02/13 15:14:18
return array(

  // sfDoctrinePlugin_module_libs

  // sfDoctrinePlugin_lib
  'sfformdoctrine' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/form/sfFormDoctrine.class.php',
  'sfformfilterdoctrine' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/form/sfFormFilterDoctrine.class.php',
  'sftesterdoctrine' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/test/sfTesterDoctrine.class.php',
  'sfdoctrinedatabase' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/database/sfDoctrineDatabase.class.php',
  'sfdoctrineconnectionprofiler' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/database/sfDoctrineConnectionProfiler.class.php',
  'sfdoctrineconnectionlistener' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/database/sfDoctrineConnectionListener.class.php',
  'sfdoctrinepager' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/pager/sfDoctrinePager.class.php',
  'sfdoctrineconfiguredatabasetask' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/task/sfDoctrineConfigureDatabaseTask.class.php',
  'sfdoctrinedatadumptask' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/task/sfDoctrineDataDumpTask.class.php',
  'sfdoctrinegeneratemigrationsdifftask' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/task/sfDoctrineGenerateMigrationsDiffTask.class.php',
  'sfdoctrinebuildsqltask' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/task/sfDoctrineBuildSqlTask.class.php',
  'sfdoctrinegeneratemoduleforroutetask' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/task/sfDoctrineGenerateModuleForRouteTask.class.php',
  'sfdoctrinecreatemodeltables' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/task/sfDoctrineCreateModelTablesTask.class.php',
  'sfdoctrinegeneratemigrationtask' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/task/sfDoctrineGenerateMigrationTask.class.php',
  'sfdoctrinecleanmodelfilestask' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/task/sfDoctrineCleanModelFilesTask.class.php',
  'sfdoctrinedataloadtask' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/task/sfDoctrineDataLoadTask.class.php',
  'sfdoctrinebuildfilterstask' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/task/sfDoctrineBuildFiltersTask.class.php',
  'sfdoctrinebuildschematask' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/task/sfDoctrineBuildSchemaTask.class.php',
  'sfdoctrinebuildtask' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/task/sfDoctrineBuildTask.class.php',
  'sfdoctrinegeneratemoduletask' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/task/sfDoctrineGenerateModuleTask.class.php',
  'sfdoctrinebuilddbtask' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/task/sfDoctrineBuildDbTask.class.php',
  'sfdoctrinemigratetask' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/task/sfDoctrineMigrateTask.class.php',
  'sfdoctrinedeletemodelfilestask' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/task/sfDoctrineDeleteModelFilesTask.class.php',
  'sfdoctrinebuildmodeltask' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/task/sfDoctrineBuildModelTask.class.php',
  'sfdoctrinegenerateadmintask' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/task/sfDoctrineGenerateAdminTask.class.php',
  'sfdoctrinebasetask' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/task/sfDoctrineBaseTask.class.php',
  'sfdoctrinegeneratemigrationsdbtask' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/task/sfDoctrineGenerateMigrationsDbTask.class.php',
  'sfdoctrinebuildformstask' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/task/sfDoctrineBuildFormsTask.class.php',
  'sfdoctrinedqltask' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/task/sfDoctrineDqlTask.class.php',
  'sfdoctrineinsertsqltask' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/task/sfDoctrineInsertSqlTask.class.php',
  'sfdoctrinedropdbtask' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/task/sfDoctrineDropDbTask.class.php',
  'sfdoctrinegeneratemigrationsmodelstask' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/task/sfDoctrineGenerateMigrationsModelsTask.class.php',
  'sfwidgetformdoctrinechoice' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/widget/sfWidgetFormDoctrineChoice.class.php',
  'sfdoctrineexception' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/sfDoctrineException.class.php',
  'sfwebdebugpaneldoctrine' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/debug/sfWebDebugPanelDoctrine.class.php',
  'swift_doctrinespool' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/mailer/Swift_DoctrineSpool.class.php',
  'sfvalidatordoctrinechoice' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/validator/sfValidatorDoctrineChoice.class.php',
  'sfvalidatordoctrineunique' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/validator/sfValidatorDoctrineUnique.class.php',
  'sfdoctrineroute' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/routing/sfDoctrineRoute.class.php',
  'sfdoctrineroutecollection' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/routing/sfDoctrineRouteCollection.class.php',
  'sfdoctrinecolumn' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/generator/sfDoctrineColumn.class.php',
  'sfdoctrinegenerator' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/generator/sfDoctrineGenerator.class.php',
  'sfdoctrineformgenerator' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/generator/sfDoctrineFormGenerator.class.php',
  'sfdoctrineformfiltergenerator' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/generator/sfDoctrineFormFilterGenerator.class.php',
  'sfdoctrinerecord' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/record/sfDoctrineRecord.class.php',
  'sfdoctrinerecordi18nfilter' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/record/sfDoctrineRecordI18nFilter.class.php',
  'sfdoctrinecli' => '/lib/vendor/symfony/lib/plugins/sfDoctrinePlugin/lib/cli/sfDoctrineCli.class.php',

  // project
  'jobeetaffiliateformfilter' => '/home/sfprojects/jobeet/lib/filter/doctrine/JobeetAffiliateFormFilter.class.php',
  'jobeetcategoryformfilter' => '/home/sfprojects/jobeet/lib/filter/doctrine/JobeetCategoryFormFilter.class.php',
  'baseformfilterdoctrine' => '/home/sfprojects/jobeet/lib/filter/doctrine/BaseFormFilterDoctrine.class.php',
  'jobeetjobformfilter' => '/home/sfprojects/jobeet/lib/filter/doctrine/JobeetJobFormFilter.class.php',
  'jobeetcategoryaffiliateformfilter' => '/home/sfprojects/jobeet/lib/filter/doctrine/JobeetCategoryAffiliateFormFilter.class.php',
  'basejobeetaffiliateformfilter' => '/home/sfprojects/jobeet/lib/filter/doctrine/base/BaseJobeetAffiliateFormFilter.class.php',
  'basejobeetcategoryaffiliateformfilter' => '/home/sfprojects/jobeet/lib/filter/doctrine/base/BaseJobeetCategoryAffiliateFormFilter.class.php',
  'basejobeetcategoryformfilter' => '/home/sfprojects/jobeet/lib/filter/doctrine/base/BaseJobeetCategoryFormFilter.class.php',
  'basejobeetjobformfilter' => '/home/sfprojects/jobeet/lib/filter/doctrine/base/BaseJobeetJobFormFilter.class.php',
  'baseform' => '/home/sfprojects/jobeet/lib/form/BaseForm.class.php',
  'jobeetcategoryform' => '/home/sfprojects/jobeet/lib/form/doctrine/JobeetCategoryForm.class.php',
  'jobeetjobform' => '/home/sfprojects/jobeet/lib/form/doctrine/JobeetJobForm.class.php',
  'jobeetcategoryaffiliateform' => '/home/sfprojects/jobeet/lib/form/doctrine/JobeetCategoryAffiliateForm.class.php',
  'baseformdoctrine' => '/home/sfprojects/jobeet/lib/form/doctrine/BaseFormDoctrine.class.php',
  'jobeetaffiliateform' => '/home/sfprojects/jobeet/lib/form/doctrine/JobeetAffiliateForm.class.php',
  'basejobeetcategoryform' => '/home/sfprojects/jobeet/lib/form/doctrine/base/BaseJobeetCategoryForm.class.php',
  'basejobeetcategoryaffiliateform' => '/home/sfprojects/jobeet/lib/form/doctrine/base/BaseJobeetCategoryAffiliateForm.class.php',
  'basejobeetaffiliateform' => '/home/sfprojects/jobeet/lib/form/doctrine/base/BaseJobeetAffiliateForm.class.php',
  'basejobeetjobform' => '/home/sfprojects/jobeet/lib/form/doctrine/base/BaseJobeetJobForm.class.php',

  // project_model
  'jobeetcategoryaffiliatetable' => '/home/sfprojects/jobeet/lib/model/doctrine/JobeetCategoryAffiliateTable.class.php',
  'jobeetaffiliate' => '/home/sfprojects/jobeet/lib/model/doctrine/JobeetAffiliate.class.php',
  'jobeetcategorytable' => '/home/sfprojects/jobeet/lib/model/doctrine/JobeetCategoryTable.class.php',
  'jobeetcategoryaffiliate' => '/home/sfprojects/jobeet/lib/model/doctrine/JobeetCategoryAffiliate.class.php',
  'jobeetaffiliatetable' => '/home/sfprojects/jobeet/lib/model/doctrine/JobeetAffiliateTable.class.php',
  'jobeetjobtable' => '/home/sfprojects/jobeet/lib/model/doctrine/JobeetJobTable.class.php',
  'jobeetjob' => '/home/sfprojects/jobeet/lib/model/doctrine/JobeetJob.class.php',
  'jobeetcategory' => '/home/sfprojects/jobeet/lib/model/doctrine/JobeetCategory.class.php',
  'basejobeetcategoryaffiliate' => '/home/sfprojects/jobeet/lib/model/doctrine/base/BaseJobeetCategoryAffiliate.class.php',
  'basejobeetjob' => '/home/sfprojects/jobeet/lib/model/doctrine/base/BaseJobeetJob.class.php',
  'basejobeetaffiliate' => '/home/sfprojects/jobeet/lib/model/doctrine/base/BaseJobeetAffiliate.class.php',
  'basejobeetcategory' => '/home/sfprojects/jobeet/lib/model/doctrine/base/BaseJobeetCategory.class.php',

  // application
  'myuser' => '/home/sfprojects/jobeet/apps/frontend/lib/myUser.class.php',

  // modules
);
