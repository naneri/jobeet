<?php
/**
 * Created by PhpStorm.
 * User: student
 * Date: 18.02.14
 * Time: 17:31
 */

class languageComponents extends sfComponents
{
    public function executeLanguage(sfWebRequest $request)
    {
        $this->form = new sfFormLanguage(
            $this->getUser(),
            array('languages' => array('en', 'fr'))
        );
    }
}