<?php
/**
 * Created by PhpStorm.
 * User: student
 * Date: 14.02.14
 * Time: 11:58
 */

class Jobeet
{
    static public function slugify($text)
    {
        // replace all non letters or digits by -
        $text = preg_replace('/\W+/', '-', $text);

        // trim and lowercase
        $text = strtolower(trim($text, '-'));

        return $text;
    }
}